import React from 'react';
import "../styles/Home.css";
import MetaData from "./MetaData";
import Footer from './Footer';
import Navbar from './Navbar';
import {Link} from "react-router-dom";

const Home = () => {
  return (
    <>
      <Navbar />
      <MetaData title={"ITT Canteen"} />
      <div className='banner'>
        <h1>Welcome to ITT Canteen</h1>
        <h4>Fresh Food and Good Moods</h4>
        <Link to="/menu"><button>EXPLORE</button></Link>
      </div>
      <Footer />
    </>
  )
}

export default Home