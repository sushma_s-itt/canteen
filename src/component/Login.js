import React from 'react';
import "../styles/Login.css";
import { Link } from 'react-router-dom';
function Login() {

    return (
        <>

            <div class="center">
                <h1>Login</h1>
                <form method="post">
                    <div class="txt_field">
                        <input type="text" required />
                        <span></span>
                        <label>Username</label>
                    </div>
                    <div class="txt_field">
                        <input type="password" required/>
                            <span></span>
                            <label>Password</label>
                    </div>
                    <div class="pass">Forgot Password?</div>
                    <Link to="/home"><input type="submit" value="Login" /></Link>
                    <div class="signup_link">
                        Not a member? <a href="#">Signup</a>
                    </div>
                </form>
            </div>

        </>
    )
}

export default Login

