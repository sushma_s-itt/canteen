import React, { Component } from 'react';
import { ProductConsumer } from "../context";
import "../styles/Product.css";
export default class Product extends Component {
  render() {
    const { id, name, time, images, price, inCart } = this.props.product;
    return (
      <div className="col-9 mx-auto col-md-6 col-lg-3 my-3">
        <div className="productCard">
          <ProductConsumer>
            {value => {
              return (
                <div
                  className="containerCt"
                >
                <div >
                <img src={images} alt="" className='prodImg' />
                  <button
                    className="cart-btn"
                    disabled={inCart ? true : false}
                  >
                    {inCart ? (
                      <p className="text-capitalize mb-0" >
                        in cart
                      </p>
                    ) : (
                      <i class="fa-solid fa-cart-plus"></i>
                    )}
                  </button>
                </div>
                </div>
              );
            }}
          </ProductConsumer>
          <div className="card-footer d-flex justify-content-between">
            <div className="tp">
              <strong className="align-self-center mb-0">{name}</strong>
              <p className="align-self-center mb-0">{time}</p>
            </div>
          <div className="price"><strong>{price} Rs</strong></div>
        </div>
      </div>
      </div>
    );
  }
}


