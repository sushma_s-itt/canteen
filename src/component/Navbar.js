import React, {Component} from 'react';
import "../styles/Navbar.css";
// import {logo} from "C:\Users\sushma.s\Desktop\canteen\public\logo.svg";
// import {logo} from "../data/data.json";
import { Link } from 'react-router-dom';

function Navbar() {
  return (
    <div className='nav'> 
       <div className='logo'>
               {/* <img src={logo} alt="Not" width={"58px"} height={"58px"}/>  */}
               {/* <img src = {logo}/> */}
               <div>ITT Canteen</div>
       </div> 
       <Link to="/home" className="navlinks"><i className="fas fa-home"></i> Home</Link>
       <Link to="/menu" className="navlinks">Menu</Link>
       <div className='searchbox navlinks'>
            <input type="text" name="search" placeholder = "search food"/> <i class="fa-solid fa-magnifying-glass"></i>
       </div>
       <Link to="/cart" className="bag"><i class="fa-solid fa-cart-shopping"></i> Cart</Link>
       <a   className="navlinks">Order History</a>
       <Link to="/"  id="log" className="navlinks"><i class="fa-solid fa-user"></i> Logout</Link>
    </div>
  )
}

export default Navbar