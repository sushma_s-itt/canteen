import React, { Component } from 'react';
import Navbar from './Navbar';
import Product from "./Product";
import Title from "./Title";
import { ProductConsumer } from '../context';

export default class ProductList extends Component {
  render() {
    return (
      <>
        <Navbar />
        <div className="title">
          <Title title="Our Menu" />
        </div>
        <div className="container">
          <ProductConsumer>
            {value => {
                return value.products.map(product => {
                return <Product key={product.id} product={product} />;
              });
            }}
          </ProductConsumer>
        </div>
      </>
    )
  }
}
