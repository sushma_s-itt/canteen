import React from "react";
import "../styles/Title.css"
export default function Title({title}) {
  return (
    <div className="row">
        <h1> <strong> {title}</strong></h1>
    </div>
  );
}