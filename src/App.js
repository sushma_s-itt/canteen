import React, { useState } from "react";
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from "./component/Home.js";
import Login from "./component/Login.js";
import Cart from "./component/Cart/Cart.js"
import ProductList from "./component/ProductList";
import "bootstrap/dist/css/bootstrap.min.css";
function App() {
  return (
    <>
      <Router>
     
          <Route exact path="/" component={Login} />
          <Route exact path="/home" component={Home} />
          <Route exact path="/menu" component={ProductList} />
          <Route exact path="/cart" component={Cart} />
      </Router>
    </>
  );
}

export default App;
